package ${conf.basePackage}.${table.camelName}.dao;

import com.jeasy.base.dao.BaseDAO;
import ${conf.basePackage}.${table.camelName}.model.${table.className};

/**
 * ${table.comment}
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
public interface ${table.className}DAO extends BaseDAO<${table.className}>{
}
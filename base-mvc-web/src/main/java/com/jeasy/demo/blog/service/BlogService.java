package com.jeasy.demo.blog.service;

import com.jeasy.base.service.BaseService;
import com.jeasy.demo.blog.model.Blog;

/**
 * 
 * @author taomk
 * @version 1.0
 * @since 2016/01/05 17:13
 */
public interface BlogService extends BaseService<Blog>{

}
